export type ItemInfo = {
  title: string
  genre: string | undefined,
  authors: string[],
  summary: string | undefined,
  publisher: string | undefined,
  image: string,
  buylink: string,
  publicationDate: string | undefined,
  isbn10: string | undefined,
}


export type BookInfo = {
  kind: string,
  totalItems: number,
  items: {
    volumeInfo: {
      title: string,
      authors: string[],
      publisher: string,
      publicationDate: string,
      description: string,
      categories: string[],
      imageLinks: {
        thumbnail: string
      },
      buylink: string,
      industryIdentifiers: {
        type: string,
        identifier: string
      }[]
    }
  }[]
}


type IndustryIdentifiers = {
  type: string,
  identifier: string
}[]


export const getGoogleBooksInfo = async (titlesArr: string[]) => {
  return await Promise.all(titlesArr.map(async (title) => {
    const response = await fetch(`https://www.googleapis.com/books/v1/volumes?q=${title}&langRestrict=en&maxResults=1`)
    if (response.ok) {
      const bookInfo: BookInfo = await response.json() as BookInfo;
      const bookObj = bookInfo.items[0]
      if (bookObj === undefined) return;
      return {
        title: bookObj.volumeInfo.title,
        genre: bookObj.volumeInfo.categories ? bookObj.volumeInfo.categories[0] : "",
        authors: bookObj.volumeInfo.authors,
        summary: bookObj.volumeInfo.description,
        publisher: bookObj.volumeInfo.publisher,
        image: bookObj.volumeInfo.imageLinks ? bookObj.volumeInfo.imageLinks.thumbnail : "",
        buylink: bookObj.volumeInfo.buylink,
        publicationDate: bookObj.volumeInfo.publicationDate,
        isbn10: bookObj.volumeInfo.industryIdentifiers
          ? getIsbn10(bookObj.volumeInfo.industryIdentifiers)
          : undefined,
      }
    }
  }))
}


const getIsbn10 = (industryIdentifiers: IndustryIdentifiers) => {
  for (const identifier of industryIdentifiers) {
    if (identifier.type === "ISBN_10") {
      return identifier.identifier;
    }
  }
}
