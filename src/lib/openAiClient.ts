import { generateText } from 'ai';
import { createOpenAI } from '@ai-sdk/openai';

const openai = createOpenAI({
  compatibility: 'strict', // strict mode, enable when using the OpenAI API
  apiKey: process.env.OPENAI_API_KEY,
});

export async function getRecommendedBooks(prevBooks: string[], genre: string) {
  const text = await generateText({
    model: openai('gpt-3.5-turbo'),
    prompt: `Based on these titles ${prevBooks.join(',')}, which is a users' last read books and the genre ${genre} he is intrested in, suggest maximum 3 book titles 
		that this user may lizke in the below JSON format. Do not send any of the last read books in the response.
		
		sample output JSON:
		[ "The White Tiger",  "A House For Mr. Biswas" ]
		
		Answer:`,
    maxTokens: 250
  })
  return text;
}

export async function getVideoRecommendations(topic: string) {
  const text = await generateText({
    model: openai('gpt-3.5-turbo'),
    prompt: ` You are an intelligent assistant that helps users find the best possible content references for their learning needs.Given the topic provided by the user, identify the top articles that meets the following criteria:
    High - quality content with clear explanations and accurate information.
    Positive user reviews and ratings.
    Professional production quality.
    Engaging and informative delivery.
    Suitable for the user’s skill level(beginner, intermediate, advanced).
    User Topic: ${topic}
    Please provide the title, a brief description, the source of the article, and skill level required for this topic.
    suggest maximum of 2 article references in the below JSON format.
    sample output JSON:
      [{title:"Introduction to Machine Learning", description:"This article provides an overview of machine learning concepts and algorithms.", source:"source", skillLevel:"Beginner, link:"articleLink"}]
        `
    ,
    maxTokens: 300
  })
  return text;
}


