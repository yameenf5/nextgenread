"use client"
import { Loader2 } from "lucide-react";
import { Button } from "../ui/button";
import { Input } from "../ui/input";
import { Label } from "../ui/label";
import { useFormState, useFormStatus } from "react-dom";
import toast from "react-hot-toast";
import { handleGetVideoRecommendations } from "~/app/ytrecommendations/_action";


export function GetVideoRecommendationForm({ userId }: { userId: string }) {
  const [state, handlefn] = useFormState(handleGetVideoRecommendations, { status: "", message: "", recommendationId: ""});
  if(state.status === "success") toast.success(state.message ? state.message: "Recommendation generated successfully");
  if(state.status === "error") toast.error(state.message ? state.message: "Error occured while generating recommendation");

  return (
    <form className="space-y-4" action={handlefn}>
      <div>
        <Label htmlFor="intrestedTopic">Topic Intrested in</Label>
        <Input name="intrestedTopic" id="intrestedTopic" placeholder="Topic" />
      </div>
      <input type="hidden" name="userId" value={userId} />
      <SubmitButton />
    </form>
  )
}


function SubmitButton() {
  const { pending } = useFormStatus();
  return (
    <Button type="submit" disabled={pending} >{pending && <Loader2 className="mr-2 h-4 w-4 animate-spin" />} Get Recommendations</Button>
  )
}
