"use client"
import { handleGetrecommendationsFromData, } from "~/app/recommender/_actions";
import { Loader2 } from "lucide-react";
import { Button } from "../ui/button";
import { Input } from "../ui/input";
import { Label } from "../ui/label";
import { useFormState, useFormStatus } from "react-dom";
import toast from "react-hot-toast";


export function GetPreviousReadBooksDataFrom({ userId }: { userId: string }) {
  const [state, handlefn] = useFormState(handleGetrecommendationsFromData, { status: "", message: "", recommendationId: ""});
  if(state.status === "success") toast.success(state.message ? state.message: "Recommendation generated successfully");
  if(state.status === "error") toast.error(state.message ? state.message: "Error occured while generating recommendation");

  return (
    <form className="space-y-4" action={handlefn}>
      <div>
        <Label htmlFor="gener" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Genre</Label>
        <select name="genre" id="genre"
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required >
          <option value="" disabled>select</option>
          <option value="Fiction">Fiction</option>
          <option value="Romance">Romance</option>
          <option value="Action">Action</option>
          <option value="Fantacy">Fantacy</option>
          <option value="Mystery">Mystery</option>
          <option value="Horror and Thriller">Horror</option>
          <option value="True crime">Crime</option>
        </select>
      </div>
      <div>
        <Label htmlFor="book-title-1">Book Title </Label>
        <Input name="bookTitle1" id="bookTitle1" placeholder="Enter book title" required />
      </div>
      <div>
        <Label htmlFor="book-title-2">Book Title </Label>
        <Input name="bookTitle2" id="bookTitle2" placeholder="Enter book title" />
      </div>
      <div>
        <Label htmlFor="book-title-3">Book Title </Label>
        <Input name="bookTitle3" id="bookTitle3" placeholder="Enter book title" />
      </div>
      <div>
        <Label htmlFor="book-title-4">Book Title </Label>
        <Input name="bookTitle4" id="bookTitle4" placeholder="Enter book title" />
      </div>
      <div>
        <Label htmlFor="book-title-5">Book Title </Label>
        <Input name="bookTitle5" id="bookTitle5" placeholder="Enter book title" />
      </div>
      <input type="hidden" name="userId" value={userId} />
      <SubmitButton />
    </form>
  )
}


function SubmitButton() {
  const { pending } = useFormStatus();
  return (
    <Button type="submit" disabled={pending} >{pending && <Loader2 className="mr-2 h-4 w-4 animate-spin" />} Get Recommendations</Button>
  )
}
