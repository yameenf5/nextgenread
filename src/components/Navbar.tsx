import Link from "next/link";
import { SignInButton, SignedIn, SignedOut, UserButton } from '@clerk/nextjs'

export function Navbar() {
  return <>
    <header className="flex items-center justify-between px-4 lg:px-6 h-14 border-b border-gray-700 bg-black text-white fixed w-full mb-10">
      <Link className="flex items-center gap-2" href="#">
        <BookIcon className="h-6 w-6" />
        <span className="font-bold text-lg">Bookworm</span>
      </Link>
      <nav className="hidden lg:flex gap-6">
        <Link className="text-sm font-medium hover:underline underline-offset-4" href="/">
          Home
        </Link>
        <Link className="text-sm font-medium hover:underline underline-offset-4" href="/recommender">
          Recommendations(Books)
        </Link>
        <Link className="text-sm font-medium hover:underline underline-offset-4" href="/ytrecommendations">
          Recommendations(YT)
        </Link>
        <Link className="text-sm font-medium hover:underline underline-offset-4" href="/about">
          About
        </Link>
      </nav>
      <SignedOut>
        <SignInButton />
      </SignedOut>
      <SignedIn>
        <span className="text-white"><UserButton showName /></span>
      </SignedIn>
    </header>
  </>
}


function BookIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M4 19.5v-15A2.5 2.5 0 0 1 6.5 2H20v20H6.5a2.5 2.5 0 0 1 0-5H20" />
    </svg>
  )
}

