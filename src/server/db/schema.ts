// Example model schema from the Drizzle docs
// https://orm.drizzle.team/docs/sql-schema-declaration

import { relations, sql } from "drizzle-orm";
import {
  pgTableCreator,
  timestamp,
  uuid,
  varchar,
} from "drizzle-orm/pg-core";

/**
 * This is an example of how to use the multi-project schema feature of Drizzle ORM. Use the same
 * database instance for multiple projects.
 *
 * @see https://orm.drizzle.team/docs/goodies#multi-project-schema
 */
export const createTable = pgTableCreator((name) => `nextgenread_${name}`);


export const generatedRecommendations = createTable(
  "generated-Recommendation",
  {
    id: uuid("id").primaryKey().defaultRandom(),
    userId: varchar("user_id", { length: 512 }).notNull(),
    createdAt: timestamp("created_at", { withTimezone: true })
      .default(sql`CURRENT_TIMESTAMP`)
      .notNull(),
    generationBasedOn: varchar("generation_based_on", { length: 256 }).notNull(),
  }
)

export const generatedRecommendationsRelation = relations(generatedRecommendations, ({ many }) => (
  {
    books: many(generatedRecommendedBooks)
  }
))

export const generatedRecommendedBooks = createTable(
  "generated-Recommended-Books",
  {
    id: uuid("id").primaryKey().defaultRandom(),
    userId: varchar("user_id", { length: 512 }).notNull(),
    recommendationId: uuid("recommendation_id").notNull(),
    title: varchar("title", { length: 128 }).notNull(),
  }
)

export const generatedBooksRelation = relations(generatedRecommendedBooks, ({ one }) => (
  {
    recommendationBasedOn: one(generatedRecommendations, {
      fields: [generatedRecommendedBooks.recommendationId],
      references: [generatedRecommendations.id]
    })
  }
))


export const generatedVideoRecommendations = createTable(
  "generated-Video-Recommendations",
  {
    id: uuid("id").primaryKey().defaultRandom(),
    userId: varchar("user_id", { length: 512 }).notNull(),
    createdAt: timestamp("created_at", { withTimezone: true })
      .default(sql`CURRENT_TIMESTAMP`)
      .notNull(),
    generationBasedOn: varchar("generation_based_on", { length: 256 }).notNull(),
  }
)

export const generatedVedioRecommendationsRelation = relations(generatedVideoRecommendations, ({ many }) => (
  {
    videos: many(generatedRecommendedVideos)
  }
))



export const generatedRecommendedVideos = createTable(
  "generated-Recommended-Videos",
  {
    id: uuid("id").primaryKey().defaultRandom(),
    userId: varchar("user_id", { length: 512 }).notNull(),
    recommendationId: uuid("recommendation_id").notNull(),
    title: varchar("title", { length: 128 }).notNull(),
    description: varchar("description", { length: 1024 }).notNull(),
    source: varchar("source", { length: 256 }).notNull(),
    skillLevel: varchar("skill_level", { length: 128 }).notNull(),
    link: varchar("link", { length: 256 }).notNull(),
  }
)




export const generatedVideoRelation = relations(generatedRecommendedVideos, ({ one }) => (
  {
    recommendationBasedOn: one(generatedVideoRecommendations, {
      fields: [generatedRecommendedVideos.recommendationId],
      references: [generatedVideoRecommendations.id]
    })
  }
))

