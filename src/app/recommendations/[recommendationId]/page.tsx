import { auth } from "@clerk/nextjs/server";
import { and, eq } from "drizzle-orm";
import Image from "next/image";
import Link from "next/link";
import { Button } from "~/components/ui/button";
import { getGoogleBooksInfo, type ItemInfo } from "~/lib/getbookinfo";
import { db } from "~/server/db";
import { generatedRecommendedBooks } from "~/server/db/schema";

export default async function GeneratedBooks({ params }: { params: { recommendationId: string } }) {
  const { userId } = auth()
  if (userId === null) return null;
  const recommendedBooks = await db.query.generatedRecommendedBooks.findMany({ where: and(eq(generatedRecommendedBooks.userId, userId), eq(generatedRecommendedBooks.recommendationId, params.recommendationId)), });
  if (recommendedBooks.length === 0) return null;
  const bookInfoArray = await getGoogleBooksInfo(recommendedBooks.map(book => book.title));

  return <>
    <div className="flex flex-col-reverse  md:flex-row min-h-screen w-full bg-black ">
      <div className="flex-1 overflow-auto p-4 sm:p-8 md:pr-0 md:order-2">
        <div className="grid grid-cols-1 gap-6 overflow-auto pr-4 sm:pr-8 scrollbar-thin scrollbar-thumb-gray-300 dark:scrollbar-thumb-gray-600 lg:mt-28 mt-16">
          <div className="rounded-lg bg-white p-6 shadow-lg dark:bg-gray-900">
            <h2 className="text-2xl font-bold">Recommended Books </h2>
            <div className="mt-4 grid grid-cols-1 gap-4 sm:grid-cols-2 lg:grid-cols-3 ">
              {bookInfoArray.map((b) => (b !== undefined ? <Book bookInfo={b} key={b.isbn10} /> : null))}
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
}


function Book({ bookInfo }: { bookInfo: ItemInfo }) {
  return (
    <div className="rounded-lg bg-gray-100 p-4 dark:bg-gray-800 flex flex-col justify-between">
      <div className="grid grid-cols-3 ">
        <div >
          <Image src={bookInfo.image !== "" ? bookInfo.image : "/NoImagePlaceholder.svg.webp"} alt="Book Cover" className="mb-4 h-auto w-auto rounded-lg object-cover" width={200} height={200} />
        </div>
        <div className="col-span-2 m-5">
          <h3 className="text-lg font-bold">{bookInfo.title}</h3>
          <br />
          <p className="text-sm font-mono">Authors:- {bookInfo.authors}</p>
          <br />
          <p className="text-sm font-mono">Genre:- {bookInfo.genre ? bookInfo.genre : "N/A"} </p>
          <br />
          <p className="text-sm font-mono">Discription:- {bookInfo.summary ? bookInfo.summary : "N/A"} </p>
          <br />
          <p className="text-sm font-mono">Published by {bookInfo.publisher ? bookInfo.publisher : "N/A"} </p>
        </div>
      </div>
      <div className="text-center ">
        <Link href={`https://www.amazon.com/s?k=${bookInfo.title}`}>
          <Button className="w-1/2 align-bottom" >Buy</Button>
        </Link>
      </div>
    </div>
  )
}

