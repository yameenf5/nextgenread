import { auth } from "@clerk/nextjs/server"
import Link from "next/link"
import { GetPreviousReadBooksDataFrom } from "~/components/forms/GetPreviousReadBooksDataForm"
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "~/components/ui/card"
import { db } from "~/server/db"

export default async function Component() {
  const { userId } = auth();
  console.log("userId is", userId)
  if (userId === null) return;
  const recommendations = await db.query.generatedRecommendations.findMany({ where: (generatedRecommendations, { eq }) => eq(generatedRecommendations.userId, userId) })

  return (
    <>
      <div className="flex flex-col-reverse md:flex-row min-h-screen w-full pt-20 bg-black">
        <div className="flex-1 overflow-auto p-4 sm:p-8 md:pr-0 md:order-2">
          <div className="grid grid-cols-1 gap-6 overflow-auto pr-4 sm:pr-8 scrollbar-thin scrollbar-thumb-gray-300 dark:scrollbar-thumb-gray-600">
            <div className="rounded-lg bg-white p-6 dark:bg-gray-900">
              <div className="text-black text-2xl">Your Recommendations:- </div>
              <div className="mt-4 grid grid-cols-1 gap-4 sm:grid-cols-2 lg:grid-cols-3">
                {recommendations.map(recommendations => <PreviouslyGeneratedRecommendations key={recommendations.id} basedOn={recommendations.generationBasedOn} recommendationId={recommendations.id} />)}
              </div>
            </div>
          </div>
        </div>
        <div className="flex w-full max-w-md flex-col items-center  p-4 sm:p-8 md:p-8 md:order-1">
          <Card className="w-full">
            <CardHeader>
              <CardTitle>Book Recommendations</CardTitle>
              <CardDescription>
                Enter the titles of books you have read, and we&apos;ll provide personalized recommendations.
              </CardDescription>
            </CardHeader>
            <CardContent>
              <GetPreviousReadBooksDataFrom userId={userId} />
            </CardContent>
          </Card>
        </div>
      </div>
    </>
  )
}


function PreviouslyGeneratedRecommendations({ basedOn, recommendationId }: { basedOn: string, recommendationId: string }) {
  const books = basedOn.split(',');
  return (
    <Link
      href={`/recommendations/${recommendationId}`}
      className="rounded-lg bg-gray-100 p-4 dark:bg-gray-800 hover:bg-gray-200 dark:hover:bg-gray-700 transition-colors"
      prefetch={false}
    >
      <div className="flex items-center gap-4">
        <div>
          <h3 className="text-lg font-bold">Recomendations based on books:- </h3>
          <div className="px-4 py-2">
            <ol>{books.length !== 0 && books.map((book, idx) => <> <li key={recommendationId+idx} className="text-xs uppercase font-bold font-mono list-decimal"> {book} </li></>)}</ol>
          </div>
        </div>
      </div>
    </Link >
  )
}
