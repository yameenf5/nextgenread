"use server"
import { revalidatePath } from "next/cache";
import { z } from "zod"
import { getRecommendedBooks } from "~/lib/openAiClient";
import { db } from "~/server/db";
import { generatedRecommendations, generatedRecommendedBooks } from "~/server/db/schema";


function getBooksIntoSingleString(books: string[]) {
  if (books.length === 0) return "";
  const booksString = books.filter(book => book !== "").join(",");
  return booksString;
}

export async function handleGetrecommendationsFromData(prevstate: { status: string, message: string | undefined, recommendationId: string | undefined }, formdata: FormData) {
  try {
    const schema = z.object({
      genre: z.string().trim().min(1, { message: "Genre Not Selected" }),
      bookTitle1: z.string().trim().min(1, { message: "At least one title is Required" }),
      bookTitle2: z.string(),
      bookTitle3: z.string(),
      bookTitle4: z.string(),
      bookTitle5: z.string(),
      userId: z.string()
    })
    const data = schema.parse(Object.fromEntries(formdata.entries()))
    const booksString = getBooksIntoSingleString([data.bookTitle1, data.bookTitle2, data.bookTitle3, data.bookTitle4, data.bookTitle5]);
    const generateRecommendations = await getRecommendedBooks([data.bookTitle1, data.bookTitle2, data.bookTitle3, data.bookTitle4, data.bookTitle5], data.genre)
    const books = JSON.parse(generateRecommendations.text) as string[];
    const insertedRecommendation = await db.insert(generatedRecommendations).values({ userId: data.userId, generationBasedOn: booksString }).returning();
    if (books.length !== 0 && insertedRecommendation.length !== 0 && insertedRecommendation[0]?.id !== undefined) {
      const recommendationId = insertedRecommendation[0]?.id;
      if (recommendationId === undefined) return { status: "error", message: "Internal Server Error", recommendationId: undefined };
      for (const book of books) {
        await db.insert(generatedRecommendedBooks).values({ userId: data.userId, recommendationId: recommendationId, title: book });
      }
    }
    revalidatePath('/recommender')
    if (books.length === 0) return { status: "error", message: "Internal server Error", recommendationId: undefined };
    return { status: "success", message: "Recommendation Generated Successfully", recommendationId: insertedRecommendation[0]?.id };
  } catch (e) {
    if (e instanceof z.ZodError) {
      return { status: "error", message: e.errors.length > 0 ? e.errors[0]?.message : "validation error", recommendationId: undefined };
    }
    return { status: "error", message: "error", recommendationId: undefined };
  }
}
