"use server"
import { revalidatePath } from "next/cache";
import { z } from "zod"
import { getVideoRecommendations } from "~/lib/openAiClient";
import { db } from "~/server/db";
import { generatedRecommendedVideos, generatedVideoRecommendations } from "~/server/db/schema";



export async function handleGetVideoRecommendations(prevstate: { status: string, message: string | undefined, recommendationId: string | undefined }, formdata: FormData) {
  try {
    const schema = z.object({
      intrestedTopic: z.string().trim().min(1, { message: "Topic not selected" }),
      userId: z.string()
    })
    const data = schema.parse(Object.fromEntries(formdata.entries()))
    const videoRecommendation = await getVideoRecommendations(data.intrestedTopic)

    const videos = JSON.parse(videoRecommendation.text) as { title: string, description: string, source: string, skillLevel: string, link: string }[];

    const insertedVedioRecommendation = await db.insert(generatedVideoRecommendations).values({ userId: data.userId, generationBasedOn: data.intrestedTopic }).returning();

    if (videos.length !== 0 && insertedVedioRecommendation.length !== 0 && insertedVedioRecommendation[0]?.id !== undefined) {

      const recommendationId = insertedVedioRecommendation[0]?.id;
      if (recommendationId === undefined) return { status: "error", message: "Internal Server Error", recommendationId: undefined };
      for (const videoDetails of videos) {
        await db.insert(generatedRecommendedVideos).values({
          userId: data.userId, recommendationId: recommendationId, title: videoDetails.title, description: videoDetails.description, source: videoDetails.source, skillLevel: videoDetails.skillLevel, link: videoDetails.link
        });
      }
    }

    revalidatePath('/recommender')
    if (videos.length === 0) return { status: "error", message: "Internal server Error", recommendationId: undefined };
    return { status: "success", message: "Recommendation Generated Successfully", recommendationId: insertedVedioRecommendation[0]?.id };
  } catch (e) {
    if (e instanceof z.ZodError) {
      return { status: "error", message: e.errors.length > 0 ? e.errors[0]?.message : "validation error", recommendationId: undefined };
    }
    return { status: "error", message: "error", recommendationId: undefined };
  }
}
