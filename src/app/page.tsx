import { AvatarImage, AvatarFallback, Avatar } from "~/components/ui/avatar"
import { Button } from "~/components/ui/button";
import Link from "next/link";

export default function HomePage() {
  return (
    <div className="flex flex-col min-h-[100dvh] bg-black text-gray-50">
      <main className="flex-1">
        <section className="w-full py-12 md:py-24 lg:py-32 bg-black">
          <div className="container px-4 md:px-6 flex flex-col items-center text-center space-y-6">
            <h1 className="text-3xl font-bold tracking-tighter sm:text-4xl md:text-5xl lg:text-6xl">
              Personalized Book Recommendations
            </h1>
            <p className="max-w-[700px] text-gray-400 md:text-xl">
              Discover your next great read based on your reading history and preferences.
            </p>
            <Button className="animate-bounce" size="lg" variant="secondary">
              Get Started
            </Button>
          </div>
        </section>
        <section className="w-full py-12 md:py-24 lg:py-32">
          <div className="container px-4 md:px-6 grid gap-8 lg:grid-cols-2 lg:gap-16">
            <div className="space-y-4">
              <div className="inline-block rounded-lg bg-black px-3 py-1 text-sm">Personalized Recommendations</div>
              <h2 className="text-3xl font-bold tracking-tighter sm:text-4xl md:text-5xl">Tailored to Your Tastes</h2>
              <p className="text-gray-400 md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed">
                Our advanced algorithms analyze your reading history and preferences to provide personalized book
                recommendations that match your unique interests.
              </p>
            </div>
            <div className="space-y-4">
              <div className="inline-block rounded-lg bg-black px-3 py-1 text-sm">Discover New Genres</div>
              <h2 className="text-3xl font-bold tracking-tighter sm:text-4xl md:text-5xl">Expand Your Horizons</h2>
              <p className="text-gray-400 md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed">
                Explore new genres and authors you might not have discovered on your own. Our recommendations help you
                broaden your literary horizons.
              </p>
            </div>
            <div className="space-y-4">
              <div className="inline-block rounded-lg bg-black px-3 py-1 text-sm">Curated Collections</div>
              <h2 className="text-3xl font-bold tracking-tighter sm:text-4xl md:text-5xl">Hand-Picked Selections</h2>
              <p className="text-gray-400 md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed">
                Our team of expert book lovers curates themed collections to help you discover new favorites, whether
                you&apos;re in the mood for a gripping thriller or a heartwarming romance.
              </p>
            </div>
            <div className="space-y-4">
              <div className="inline-block rounded-lg bg-black px-3 py-1 text-sm">Reading Insights</div>
              <h2 className="text-3xl font-bold tracking-tighter sm:text-4xl md:text-5xl">
                Understand Your Reading Habits
              </h2>
              <p className="text-gray-400 md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed">
                Track your reading progress, discover your preferences, and get insights to help you become an even more
                avid reader.
              </p>
            </div>
          </div>
        </section>
        <section className="w-full py-12 md:py-24 lg:py-32 bg-black">
          <div className="container px-4 md:px-6 grid gap-8 lg:grid-cols-2 lg:gap-16">
            <div className="space-y-4">
              <div className="inline-block rounded-lg bg-black px-3 py-1 text-sm">What Our Customers Say</div>
              <h2 className="text-3xl font-bold tracking-tighter sm:text-4xl md:text-5xl">Loved by Bookworms</h2>
              <div className="space-y-6">
                <div className="flex flex-col gap-4">
                  <div className="flex items-center gap-4">
                    <Avatar>
                      <AvatarImage src="/placeholder-user.jpg" />
                      <AvatarFallback>JD</AvatarFallback>
                    </Avatar>
                    <div>
                      <div className="font-medium">Jane Doe</div>
                      <div className="text-gray-400 text-sm">Avid Reader</div>
                    </div>
                  </div>
                  <p className="text-gray-400 md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed">
                    &qout;Bookworm has completely transformed my reading experience. The personalized recommendations are
                    spot on, and I&apos;ve discovered so many new authors and genres that I love.&qout;
                  </p>
                </div>
                <div className="flex flex-col gap-4">
                  <div className="flex items-center gap-4">
                    <Avatar>
                      <AvatarImage src="/placeholder-user.jpg" />
                      <AvatarFallback>JD</AvatarFallback>
                    </Avatar>
                    <div>
                      <div className="font-medium">John Doe</div>
                      <div className="text-gray-400 text-sm">Book Enthusiast</div>
                    </div>
                  </div>
                  <p className="text-gray-400 md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed">
                    &qout;I never thought I&apos;d be the type of person to use a book recommendation service, but Bookworm has
                    completely changed my mind. The insights and suggestions are invaluable.&qout;
                  </p>
                </div>
              </div>
            </div>
            <div className="space-y-4">
              <div className="inline-block rounded-lg bg-black px-3 py-1 text-sm">About Bookworm</div>
              <h2 className="text-3xl font-bold tracking-tighter sm:text-4xl md:text-5xl">
                Discover Your Next Great Read
              </h2>
              <p className="text-gray-400 md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed">
                Bookworm is a SaaS platform that uses advanced algorithms and a team of expert book lovers to provide
                personalized book recommendations tailored to your unique reading preferences and habits.
              </p>
              <p className="text-gray-400 md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed">
                Our mission is to help book enthusiasts of all kinds discover new authors, genres, and literary gems
                that they&apos;ll truly love. Whether you&apos;re a voracious reader or someone who&apos;s looking to get back into
                reading, Bookworm is here to guide you on your literary journey.
              </p>
              <Button className="animate-pulse" variant="secondary">
                Learn More
              </Button>
            </div>
          </div>
        </section>
      </main>
      <footer className="flex flex-col gap-2 sm:flex-row py-6 w-full shrink-0 items-center px-4 md:px-6 border-t border-gray-700">
        <p className="text-xs text-gray-500">© 2024 Bookworm. All rights reserved.</p>
        <nav className="sm:ml-auto flex gap-4 sm:gap-6">
          <Link className="text-xs hover:underline underline-offset-4" href="#">
            Terms of Service
          </Link>
          <Link className="text-xs hover:underline underline-offset-4" href="#">
            Privacy Policy
          </Link>
        </nav>
      </footer>
    </div>
  );
}
