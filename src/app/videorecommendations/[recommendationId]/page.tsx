import { auth } from "@clerk/nextjs/server";
import { and, eq } from "drizzle-orm";
import Link from "next/link";
import { Button } from "~/components/ui/button";
import { db } from "~/server/db";
import { generatedRecommendedVideos } from "~/server/db/schema";

export default async function GeneratedBooks({ params }: { params: { recommendationId: string } }) {
  const { userId } = auth()
  if (userId === null) return null;
  const recommendedVideos = await db.query.generatedRecommendedVideos.findMany({ where: and(eq(generatedRecommendedVideos.userId, userId), eq(generatedRecommendedVideos.recommendationId, params.recommendationId)), });
  if (recommendedVideos.length === 0) return null;

  return <>
    <div className="flex flex-col-reverse  md:flex-row min-h-screen w-full bg-black ">
      <div className="flex-1 overflow-auto p-4 sm:p-8 md:pr-0 md:order-2">
        <div className="grid grid-cols-1 gap-6 overflow-auto pr-4 sm:pr-8 scrollbar-thin scrollbar-thumb-gray-300 dark:scrollbar-thumb-gray-600 lg:mt-28 mt-16">
          <div className="rounded-lg bg-white p-6 shadow-lg dark:bg-gray-900">
            <h2 className="text-2xl font-bold">Recommended Books </h2>
            <div className="mt-4 grid grid-cols-1 gap-4 sm:grid-cols-2 lg:grid-cols-3 ">
              {recommendedVideos.map((v) => (v !== undefined ? <Book videoInfo={v} key={v.id} /> : null))}
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
}


function Book({ videoInfo }: { videoInfo: typeof generatedRecommendedVideos.$inferSelect }) {
  return (
    <div className="rounded-lg bg-gray-100 p-4 dark:bg-gray-800 flex flex-col justify-between">
      <div className="grid grid-cols-3 ">
        <div className="col-span-2 m-5">
          <h3 className="text-lg font-bold">{videoInfo.title}</h3>
          <br />
          <p className="text-sm font-mono">Level:- {videoInfo.skillLevel}</p>
          <br />
          <p className="text-sm font-mono">Discription:- {videoInfo.description} </p>
          <br />
          <p className="text-sm font-mono">source by {videoInfo.source} </p>
        </div>
      </div>
      <div className="text-center ">
        <Link href={videoInfo.link}>
          <Button className="w-1/2 align-bottom" >watch</Button>
        </Link>
      </div>
    </div>
  )
}

